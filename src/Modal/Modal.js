import React, { Component } from 'react'
import ReactDOM from 'react-dom';


class Modal extends Component {

  constructor(props) {
    super(props);
    this.modalRoot = document.getElementById('modal-container');
    this.el = document.createElement('div');
  }

  componentDidMount = () => {
    this.modalRoot.appendChild(this.el);
  }

  componentWillUnmount = () => {
    // this.modalRoot.innerHTML = '';
    this.modalRoot.removeChild(this.el);
  }

  render () {
    const portal = ReactDOM.createPortal(
      this.props.children,
      this.el
    )

    return portal;
  }
}

export default Modal