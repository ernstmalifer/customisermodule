import React from 'react';

const colors = [
  { name: 'White', hex: '#FFFFFF'},
  { name: 'Baby Blue', hex: '#00C9F1'},
  { name: 'Sky Blue', hex: '#0097DD'},
  { name: 'Blue', hex: '#0073C1'},
  { name: 'Dark Blue', hex: '#004182'},
  { name: 'Indigo', hex: '#060B49'},
  { name: 'Dark Green', hex: '#00432F'},
  { name: 'Green', hex: '#008546'},
  { name: 'Yellow', hex: '#FCF900'},
  { name: 'Gold', hex: '#FFDF00'},
  { name: 'Orange', hex: '#FF7600'},
  { name: 'Crimson', hex: '#FF0036'},
  { name: 'Maroon', hex: '#990000'},
  { name: 'Purple', hex: '#560083'},
  { name: 'Pink', hex: '#D979B8'},
];

const ColorTool = props => {

  const { id, name, mesh, changeColor } = props;

  return (
    <div className="" id={`control-${id}`}>
      <h5>{name}</h5>
      {colors.map( (color, idx) => {
        return (
          <button key={`color-${idx}`} className="btn btn-color mr-1 mb-1" style={{backgroundColor: `${color.hex}`}} onClick={() => changeColor(mesh, color.hex)}>{color.name}</button>
        )
      })}
    </div>
  );
};

export default ColorTool;