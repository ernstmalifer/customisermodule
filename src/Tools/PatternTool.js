import React, { Component } from 'react';

import Modal from './../Modal/Modal';

import $ from 'jquery'; 

const samplePatterns = [
  { name: 'Transparent', url: 'http://biccustomiserportal.engagisdemo.com.au/wp-content/uploads/transparent.png'},
  { name: 'Zebra', url: 'http://biccustomiserportal.engagisdemo.com.au/wp-content/uploads/zebra.png'},
  { name: 'Cheetah', url: 'http://biccustomiserportal.engagisdemo.com.au/wp-content/uploads/cheetah.jpg'},
  { name: 'Impact', url: 'http://biccustomiserportal.engagisdemo.com.au/wp-content/uploads/impact.png'},
  { name: 'Pikachu', url: 'http://biccustomiserportal.engagisdemo.com.au/wp-content/uploads/pikachu.jpg'},
];

class PatternTool extends Component {

  state = {
    patterns: []
  }

  componentDidMount = () => {
    this.props.changePattern(this.props.mesh, samplePatterns[0].url);
  }

  loadPatterns = () => {
    this.setState({patterns: samplePatterns})
  }

  showInfoModal = () => {
    $('#patternTool-info-modal').modal('show');
  }

  hideInfoModal = () => {
    $('#patternTool-info-modal').modal('hide');
  }

  componentWillUnmount = () => {

  }

  render() {

    const { id, name, mesh, changePattern } = this.props;
    const { patterns, showInfoModal } = this.state;

    return (
      <React.Fragment>
        <div className="" id={`control-${id}`}>
          <h5>{name}</h5>
        </div>
      </React.Fragment>
    );
  }

};

export default PatternTool;