import React, { Component } from 'react';
import update from 'immutability-helper';

const colors = [
  { name: 'White', color_name: 'White'},
  { name: 'Black', color_name: 'Black'},
  { name: 'Blue', color_name: 'Blue'},
  { name: 'Green', color_name: 'Green'},
  { name: 'Yellow', color_name: 'Yellow'},
  { name: 'Orange', color_name: 'Orange'},
  { name: 'Red', color_name: 'Red'},
  { name: 'Purple', color_name: 'Purple'},
  { name: 'Pink', color_name: 'Pink'},
  { name: 'Brown', color_name: 'Brown'},
];

class TextTool extends Component {

  state = {
    text: '',
    font: {
      bold: false,
      italic: false,
      size: 30,
      style: 'Arial'
    },
    color: 'Black'
  }

  componentDidMount = () => {
    this.createText();
  }

  createText = () => {
    const font = `${this.state.font.bold ? `bold` : ``} ${this.state.font.italic ? `italic` : ``} ${this.state.font.underline ? `underline` : ``} ${this.state.font.size}px ${this.state.font.style}`;
    this.props.createText(this.props.mesh, this.state.text, font, this.state.color);
  }

  render() {

    const { id, name, mesh, createText } = this.props;

    return (
      <div className="" id={`control-${id}`}>
        <h5>{name}</h5>

        <input type="text" className="form-control" placeholder="Text" onChange={(e) => 
          {
            this.setState(update(this.state, {text: {$set: e.target.value}}), () => {
              this.createText();
            })
          }
        } />
        <button type="button" className={`btn ${this.state.font.bold ? `btn-primary` : `btn-light`}`} onClick={() => {
          this.setState(update(this.state, {font: {bold: {$set: !this.state.font.bold}}}), () => {
            this.createText();
          })
        }}>B</button>
        <button type="button" className={`btn ${this.state.font.italic ? `btn-primary` : `btn-light`}`} onClick={() => {
          this.setState(update(this.state, {font: {italic: {$set: !this.state.font.italic}}}), () => {
            this.createText();
          })
        }}>I</button>
        <select style={{width: '150px'}} className="form-control" id="font-control" value={this.state.font.value} onChange={(e) => {
          this.setState(update(this.state, {font: {style: {$set: e.target.value}}}), () => {
            this.createText();
          })
        }}>
          {
            // This might be system fonts
          }
          <option value="Arial">Arial</option>
          <option value="Century Gothic">Century Gothic</option>
          <option value="Comic Sans MS">Comic Sans MS</option>
          <option value="Georgia">Georgia</option>
          <option value="Impact">Impact</option>
          <option value="Lucida Console">Lucida Console</option>
          <option value="Monotype Corsiva">Monotype Corsiva</option>
          <option value="Times New Roman">Times New Roman</option>
          <option value="Trebuchet MS">Trebuchet MS</option>
          <option value="Verdana">Verdana</option>
        </select>
        <input style={{width: '100px'}} type="number" className="form-control" id="font-size-control" value={this.state.font.size} onChange={(e) => {
          this.setState(update(this.state, {font: {size: {$set: parseInt(e.target.value)}}}), () => {
            this.createText();
          })
        }}/>

        <div className="input-group">
          {colors.map( (color, idx) => {
            return (
              <button key={`color-${idx}`} className="btn btn-color mr-1 mb-1" style={{backgroundColor: `${color.color_name}`}} onClick={() => {     this.setState(update(this.state, {color: {$set: color.color_name}}), () => {
                  this.createText();
                })
              }}>{color.color_name}</button>
            )
          })}
        </div>
        
      </div>
    );
  }

};

export default TextTool;